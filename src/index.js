import React, { useState, useRef, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { useSpring, animated } from 'react-spring';
import ReactParticles from 'react-particles-js';
import particlesConfig from './particles-config.js';
import './styles.scss';

import { csv } from 'd3';
import { VictoryBar, VictoryChart } from 'victory';


function App() {
  return (
    <div className="main">
      <Particles>
        <Hero>
          <div className="container">
            <Info />
            <div className="row">
              {cards.map((card, i) => (
                <div className="column">
                  <Card>
                    <div className="card-title">{card.title}</div>
                    <div className="card-body">{card.description}</div>
                    <Image ratio={card.imageRatio} src={card.image} alt={card.alt} />
                  </Card>
                </div>
              ))}
            </div>
          </div>
        </Hero>
        <ChartApp />
        </Particles>
    </div>
  );
}

function Card({ children }) {
  // We add this ref to card element and use in onMouseMove event ...
  // ... to get element's offset and dimensions.
  const ref = useRef();

  // Keep track of whether card is hovered so we can increment ...
  // ... zIndex to ensure it shows up above other cards when animation causes overlap.
  const [isHovered, setHovered] = useState(false);

  const [animatedProps, setAnimatedProps] = useSpring(() => {
    return {
      // Array containing [rotateX, rotateY, and scale] values.
      // We store under a single key (xys) instead of separate keys ...
      // ... so that we can use animatedProps.xys.interpolate() to ...
      // ... easily generate the css transform value below.
      xys: [0, 0, 1],
      // Setup physics
      config: { mass: 10, tension: 400, friction: 40, precision: 0.00001 }
    };
  });

  return (
    <animated.div
      ref={ref}
      className="card"
      onMouseEnter={() => setHovered(true)}
      onMouseMove={({ clientX, clientY }) => {
        // Get mouse x position within card
        const x =
          clientX -
          (ref.current.offsetLeft -
            (window.scrollX || window.pageXOffset || document.body.scrollLeft));

        // Get mouse y position within card
        const y =
          clientY -
          (ref.current.offsetTop -
            (window.scrollY || window.pageYOffset || document.body.scrollTop));

        // Set animated values based on mouse position and card dimensions
        const dampen = 50; // Lower the number the less rotation
        const xys = [
          -(y - ref.current.clientHeight / 2) / dampen, // rotateX
          (x - ref.current.clientWidth / 2) / dampen, // rotateY
          1.07 // Scale
        ];

        // Update values to animate to
        setAnimatedProps({ xys: xys });
      }}
      onMouseLeave={() => {
        setHovered(false);
        // Set xys back to original
        setAnimatedProps({ xys: [0, 0, 1] });
      }}
      style={{
        // If hovered we want it to overlap other cards when it scales up
        zIndex: isHovered ? 2 : 1,
        // Interpolate function to handle css changes
        transform: animatedProps.xys.interpolate(
          (x, y, s) =>
            `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`
        )
      }}
    >
      {children}
    </animated.div>
  );
}

function Particles({ children }) {
  return (
    <div style={{ position: 'relative' }}>
      <ReactParticles
        params={particlesConfig}
        style={{
          position: 'absolute',
          zIndex: 1,
          left: 0,
          right: 0,
          bottom: 0,
          top: 0
        }}
      />
      {children && <div style={{ position: 'relative' }}>{children}</div>}
    </div>
  );
}

function Hero({ children }) {
  return (
    <div className="hero">
      <div className="hero-body">{children}</div>
    </div>
  );
}

function Image({ ratio, src, alt }) {
  return (
    <div className="image-container">
      <div className="image-inner-container">
        <div
          className="ratio"
          style={{
            paddingTop: ratio * 100 + '%'
          }}
        >
          <div className="ratio-inner">
            <img src={src} alt={alt} />
          </div>
        </div>
      </div>
    </div>
  );
}

function Info() {
  return (
    <div className="info">
      <h1>RSCC-Athletisme{' '}</h1>
      <a target="_blank" rel="noopener noreferrer" href="http//localhost:3000">
        rscc-athletisme.fr
      </a>
      <div className="notice">(best viewed at larger screen width)</div>
    </div>
  );
}

const cards = [
  {
    title: 'REPRISE DE LA SAISON SPORTIVE 2019-2020',
    description:
      'Prenez contact avec la section en écrivant à rsccathle@gmail.com pour toutes informations ou Venez nous rencontrer à partir de 18h sur la piste d’Athletisme',
    image: 'https://alcbrs.fr/wp-content/uploads/2018/11/qui-contacter.jpg',
    imageRatio: 784 / 1016,
    alt: 'undraw_col'
  },
  {
    title: 'GROUPES ENTRAINEMENT ET HORAIRES',
    description:
      'Baby Athle 4/5 ans : Mardi de 18h15 à 19h15 Éveils Athle 6 à 8 ans : mardi de 18h à 19h30 Poussins 9/10 ans.',
    image: 'https://www.cjfathletisme-saintmalo.com/wp-content/uploads/2019/06/CD-35-Athl%C3%A9-320x202.png',
    imageRatio: 839 / 1133,
    alt: 'undraw_col'
  },
  {
    title: 'CALENDRIER',
    description:
      "Les rendez-vous importants...",
    image: 'https://www.calendrier-365.fr/jpg/calendrier-2020-format-portrait.jpg',
    imageRatio: 730 / 1030,
    alt: 'undraw_col'
  }
];

// Portion added from Viz from https://www.youtube.com/watch?v=OacqgtI30pk using victory and D3

const row = d => {
    d.Performences = +d.Performences;
    return d;
  };

const ChartApp = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        csv('BDDRdCl.test.csv', row).then(setData);
    }, []);

    console.log(data);

    return (
      <div className="chart">
        <VictoryChart
          style={{ticketLabels: {fontsize: 120}}}
          width='960'
          height='500'
          domainPadding={50}
          padding={{ top: 40, bottom: 40, left: 80, right: 20 }}
        >
          <VictoryBar data={data} x="Performences" y="Epreuves" />
        </VictoryChart>
      </div>
    );
};

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
